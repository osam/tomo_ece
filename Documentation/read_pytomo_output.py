import numpy as np
from pathlib import Path
import pickle
import matplotlib.pyplot as plt
home = str(Path.home())
tomo_tmp_folder = home + "/tomography/tmp/"

fileload = "Emissivity_6.932-6.933_rec_40024.npz"
Emissivity = np.load(tomo_tmp_folder + fileload, allow_pickle=True)
# print(Emissivity.files) # to show the variables

['gres', 'tvec', 'rvec', 'zvec', 'inputs', 'gres_norm', 'BdMat']
gres, tvec, rvec, zvec, inputs, gres_norm, BdMat = Emissivity['gres'], Emissivity['tvec'], Emissivity['rvec'], Emissivity['zvec'], Emissivity['inputs'].all(), Emissivity['gres_norm'],  Emissivity['BdMat']

shot = inputs['shot']


time_idx = int(1222)
contourNN = 20
contours = plt.contourf(rvec, zvec, gres[:,:,time_idx]*gres_norm[time_idx]*1e-3, contourNN, cmap = "jet")
plt.contour(rvec, zvec, gres[:,:,time_idx]*gres_norm[time_idx]*1e-3, contourNN, cmap = "binary")
cbar = plt.colorbar(contours)
cbar.ax.tick_params(labelsize=8, rotation=90) 
cbar.ax.set_ylabel('Emissivity [kW/m^3]', rotation=90)
plt.ylabel("z [m]")
plt.xlabel("R [m]")
plt.title("Emissivity, #%g, t = %.6g s" %(shot,tvec[time_idx]))
plt.axis('equal')
plt.show()
