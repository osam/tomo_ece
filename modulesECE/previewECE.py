import sys, os, random
import matplotlib

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.qt_compat import QtCore, QtWidgets # for inline plotting (tab 1D signals)
# from matplotlib.backends.qt_compat import QtCore, QtWidgets, is_pyqt5 # for inline plotting (tab 1D signals)
from matplotlib.backends.backend_qt5agg import (
FigureCanvas, NavigationToolbar2QT as NavigationToolbar) # for inline plotting (tab 1D signals)

# except:
    # from PyQt4.QtCore import *
    # from PyQt4.QtGui import *

        
    # from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
    # try:
        # from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
    # except:
        # from matplotlib.backends.backend_qt4agg import NavigationToolbar as NavigationToolbar
        
from matplotlib.figure import Figure
# from matplotlib.backends.qt_compat import QtCore, QtWidgets, is_pyqt5
import numpy as np
import config
from collections import OrderedDict # for tab2 and tab3
from numpy import *
import time
import os,sys
import traceback # for better error handeling
from mpl_toolkits.axes_grid1.inset_locator import inset_axes # for colorbar display
import aug_sfutils as sf # for SXR load



class PreviewECEWindow(QMainWindow):
    def __init__(self, parent):
        QMainWindow.__init__(self, parent)
        self.setWindowTitle('Preview ECE data')
        self.parent = parent
        self.check_plot_exist = False

        self.create_main_frame()
        self.create_tabs()
        # check if some channels have been already deleted
        if hasattr(self.parent.ECp, 'idxs_del'):
            chs_del = self.parent.ECp.chs_all[self.parent.ECp.idxs_del]
            self.SetWrongDets(chs_del)
        self.plot_main()
        
        
    def load_data(self):
        pass

        
    def create_main_frame(self):
        

        self.cWidget = QWidget(self)

        self.gridLayout = QGridLayout(self.cWidget)

        self.setWindowTitle('Data Preview ECE')

        self.Expand = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.Fixed  = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.main_tab = QTabWidget(self)
        self.main_tab.setTabPosition(QTabWidget.North)

        self.gridLayout.addWidget(self.main_tab, 0, 0, 1,2)
        
        self.setCentralWidget(self.cWidget)
        
        
        self.resize(600, 650)
        self.setCenter()
        

    def create_tabs(self):

        self.tokamak = self.parent.tokamak
        self.setting = self.parent.setting
        
        self.tab_widget_data = QWidget(self)
        self.tab_ECESXR = QWidget(self)
        
        #self.parent = tok()

        self.main_tab.insertTab(0,self.tab_widget_data, 'Plots')
        self.main_tab.insertTab(1,self.tab_ECESXR, 'ECE-SXR compare')

        self.tab_widget_data.setSizePolicy(self.Expand)
        self.tab_widget_data.setMinimumSize(900,600)
        self.tab_ECESXR.setSizePolicy(self.Expand)



# ----------------------------------------------------------------------------------
        # Plot ECE trace view layout
        self.verticalLayout_data = QVBoxLayout(self.tab_widget_data)
        self.horizontalLayout_data = QHBoxLayout()

        self.figure_main = Figure(figsize=(5, 3))
        self.static_canvas_main = FigureCanvas(self.figure_main)
        
        self.verticalLayout_data.addWidget(
            self.static_canvas_main,
            QtCore.Qt.AlignTop)  # align the plot up
        self.verticalLayout_data.addStretch()  # stretch plot in all free space
        self.toolbar_main = NavigationToolbar(
            self.static_canvas_main,
            self.tab_widget_data,
            coordinates=True)  # add toolbar below the plot
        
        self.verticalLayout_data.addWidget(self.toolbar_main)
        self._static_ax_main = self.static_canvas_main.figure.subplots(
            1, 2, sharex=False, sharey=False)  # add axes

        #prepare first data
        self.groupBox = QGroupBox("")
        self.gridLayout = QGridLayout(self.groupBox)
        self.labelDetectors = QLabel( "Wrong detectors(format 2,3,9:14):")
        self.Edit_wrongDetectors = QLineEdit()
        self.refreshButton=QPushButton("Reload the data")
        self.gridLayout.addWidget(self.labelDetectors, 0, 0)
        self.gridLayout.addWidget(self.Edit_wrongDetectors, 0, 1)
        self.gridLayout.addWidget(self.refreshButton, 0, 2)

        self.verticalLayout_data.addWidget(self.groupBox)



        self.labelInterval = QLabel( "Time range [s]: ")
        self.labelFrom= QLabel( "from")
        
        
        self.tmin_lineEdit = QLineEdit()
        self.tmax_lineEdit = QLineEdit()

        self.labelTo = QLabel( "to")


        spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, 
                                       QSizePolicy.Minimum)

        self.OkButton=QPushButton("OK")

        self.hLayout = QHBoxLayout()
        self.hLayout.addWidget(self.labelFrom)
        self.hLayout.addWidget(self.tmin_lineEdit)
        self.hLayout.addWidget(self.labelTo)
        self.hLayout.addWidget(self.tmax_lineEdit)
        self.hLayout.addItem(spacerItem)
        
        self.gridLayout.addWidget(self.labelInterval, 1, 0)
        self.gridLayout.addLayout(self.hLayout, 1, 1)
        self.gridLayout.addWidget(self.OkButton, 1, 2)


        # tooltips
        self.resize(400, 400)

        frect = QDesktopWidget.frameGeometry(self)
        frect.moveCenter(QDesktopWidget().availableGeometry(self.parent).center());
        self.move(frect.topLeft())

        # self.tmin_lineEdit.setText(str(double(self.parent.tmin_wideECE)))
        # self.tmax_lineEdit.setText(str(double(self.parent.tmax_wideECE)))
        self.tmin_lineEdit.setText("%0.7g"%self.parent.tmin_wideECE)
        self.tmax_lineEdit.setText("%0.7g"%self.parent.tmax_wideECE)
        self.tmin_lineEdit.setReadOnly(True)
        self.tmax_lineEdit.setReadOnly(True)

        # self.RefreshEvent(True)

        self.OkButton.clicked.connect(self.CloseEvent)
        
        self.closeEvent = self.CloseEvent
        self.refreshButton.clicked.connect(self.RefreshEvent)

# ----------------------------------------------------------------------------------

        # ECE_SXR tab - content
        # Create layouts
        
        # self.tab_ECESXR.setSizePolicy(self.Expand)
        self.verticalLayout_ECESXR = QVBoxLayout(self.tab_ECESXR)
        self.horizontalLayout_ECESXR = QHBoxLayout()

        self.figure_ECESXR = Figure(figsize=(5, 3))
        self.static_canvas_ECESXR = FigureCanvas(self.figure_ECESXR)
        
        self.verticalLayout_ECESXR.addWidget(
            self.static_canvas_ECESXR,
            QtCore.Qt.AlignTop)  # align the plot up
        self.verticalLayout_ECESXR.addStretch()  # stretch plot in all free space
        self.toolbar_ECESXR = NavigationToolbar(
            self.static_canvas_ECESXR,
            self.tab_ECESXR,
            coordinates=True)  # add toolbar below the plot
        
        self.verticalLayout_ECESXR.addWidget(self.toolbar_ECESXR)
        self._static_ax_ECESXR = self.static_canvas_ECESXR.figure.subplots(
            1, 1, sharex=False, sharey=False)  # add axes

        #buttons and edits
        self.groupBox_ECESXR = QGroupBox("")
        self.gridLayout_ECESXR = QGridLayout(self.groupBox_ECESXR)

        self.label_fmn_ECESXR = QLabel("f_(m,n) [kHz]:")
        # self.mode_f_preview = float(self.mode_f_lineEdit.text()) # (Hz)
        # self.mode_f_preview = self.mode_f_lineEdit.text()
        self.lineEdit_fmn_ECESXR = QLineEdit(self.parent.mode_f_lineEdit.text()) 
        self.reloadButton_ECESXR = QPushButton("Change f_(m,n) and reload ECE")
        self.reloadButton_ECESXR.clicked.connect(self.reload_new_fmode)
        self.gridLayout_ECESXR.addWidget(self.label_fmn_ECESXR, 0, 0)
        self.gridLayout_ECESXR.addWidget(self.lineEdit_fmn_ECESXR, 0, 1)
        self.gridLayout_ECESXR.addWidget(self.reloadButton_ECESXR, 0, 2,1,2)


        self.labelSXR_ECESXR = QLabel("SXR LOS:")
        self.chooseSXR_ECESXR = QComboBox(self.tab_ECESXR)

        self.SXR_array = ['SlowSXR_I_054',
                        'FastSXR_I_054']
        self.chooseSXR_ECESXR.addItems(self.SXR_array)

        self.labelECE_ECESXR = QLabel("ECE channel:")
        self.labelECE_ECESXR.setMinimumWidth(100)
        self.chooseECE_ECESXR = QComboBox(self.tab_ECESXR)
        self.chooseECE_ECESXR.addItems(self.parent.ECp.chs.astype(str))
        self.chooseECE_ECESXR.setMinimumWidth(300)

        self.gridLayout_ECESXR.addWidget(self.labelSXR_ECESXR, 1, 0)
        self.gridLayout_ECESXR.addWidget(self.chooseSXR_ECESXR, 1, 1)
        self.gridLayout_ECESXR.addWidget(self.labelECE_ECESXR, 1, 2)
        self.gridLayout_ECESXR.addWidget(self.chooseECE_ECESXR, 1, 3)

        self.plotButton_ECESXR = QPushButton("Load and plot the data")
        self.plotButton_ECESXR.clicked.connect(self.plot_func_ECESXR)
        self.gridLayout_ECESXR.addWidget(self.plotButton_ECESXR, 2, 0, 1, 4)




        self.verticalLayout_ECESXR.addWidget(self.groupBox_ECESXR)

        # self.verticalLayout_ECESXR.addWidget(self.plotButton_ECESXR)
        # self.verticalLayout_ECESXR.addWidget(self.label_fmn_ECESXR)
        # self.verticalLayout_ECESXR.addWidget(self.plotButton_ECESXR)
        # self.labelDetectors = QLabel( "Wrong detectors(format 2,3,9:14):")
        # self.Edit_wrongDetectors = QLineEdit()

# ----------------------------------------------------------------------------------
    def plot_main(self):
        try:

            tTrace = zeros_like(self.parent.ECp.rhop)
            for jj in range(self.parent.ECp.rhop.shape[1]):
                tTrace[:,jj] = self.parent.ECp.time
            rrTrace = self.parent.ECp.rhop
            TeTrace = self.parent.ECp.Te
            
            self.parent.ECp.getDominantFreq_FFT()
            dom_f_arr_FFT = self.parent.ECp.dom_f_arr_FFT
            chs = self.parent.ECp.chs
            
            contourNN_A = 40
            if (self.check_plot_exist == True):
                self._static_ax_main[0].clear()
                self._static_ax_main[1].clear()
                self.cbar.remove()
                
            contoursA = self._static_ax_main[0].contourf(
                tTrace, rrTrace, TeTrace, levels=contourNN_A, cmap="jet")
            self._static_ax_main[0].contour(
                tTrace, rrTrace, TeTrace, levels=contourNN_A, cmap="binary", linewidths = 0.6)

            
            # axins = inset_axes(
                # self._static_ax_main[0],
                # width="3%",  # width: 5% of parent_bbox width
                # height="100%",  # height: 50%
                # loc="upper left",
                # bbox_to_anchor=(1.01, 0., 1, 1),
                # bbox_transform=self._static_ax_main[0].transAxes,
                # borderpad=0,
            # )
            
            axins = inset_axes(self._static_ax_main[0],
                    width="100%",  
                    height="2%",
                    loc='lower center',
                    borderpad=-3
                   )
            
            self.cbar = self.figure_main.colorbar(contoursA, cax=axins, orientation="horizontal")
            self.cbar.ax.tick_params(labelsize=8, rotation=0) 
            self.cbar.ax.set_ylabel('Trad [eV]', fontsize = 8, rotation=90)
            
            
            t_ones = self.parent.ECp.time[0]*np.ones(self.parent.ECp.rhop.shape[1])
            dt_shift = 0.02*(self.parent.ECp.time[-1] - self.parent.ECp.time[0])
            self._static_ax_main[0].plot(t_ones - dt_shift, self.parent.ECp.rhop[0,:], "ko")
            my_text = self.parent.ECp.chs
            for i, txt in enumerate(my_text):
                self._static_ax_main[0].annotate(txt, (t_ones[i] - dt_shift,self.parent.ECp.rhop[0,i]),fontsize=12)
            
            # self.parent.ECp.time[0]
            self._static_ax_main[0].set_ylabel(r"$\rho_{pol}$")
            self._static_ax_main[0].set_xlabel("time [s]")
            self._static_ax_main[0].set_title("Shot: %g" %(self.parent.shot))
            

            # rename axes
            # names_xaxis = np.zeros_like(chs).astype
            names_yaxis = chs.copy().astype(str)
            y_ticks = np.arange(len(chs))  # the label locations
            self._static_ax_main[1].barh(y_ticks,
                dom_f_arr_FFT,
                color="black",
                capsize=8,
                )
            
            self._static_ax_main[1].set_yticks(y_ticks)
            self._static_ax_main[1].set_yticklabels(names_yaxis)
            self._static_ax_main[1].set_ylabel("ECE channels")
            self._static_ax_main[1].set_xlabel("freq [Hz]")
            self._static_ax_main[1].set_title("Dominant frequency FFT")
            self._static_ax_main[1].xaxis.grid(alpha=1.0)
            
            
            self.static_canvas_main.draw()
            self.check_plot_exist = True

        except Exception as exc:
            # Handle the error gracefully
            tb = traceback.format_exc()
            print(f"An error occurred: {exc}\nTraceback:\n{tb}")

    def plot_func_ECESXR(self):
        try:
            ### SXR load
            SXR_choice = self.chooseSXR_ECESXR.currentText()
            if (SXR_choice == "SlowSXR_I_054"):
                SXRdiag = "SSX"
                SXRsig = "I_054"
                sfSXR = sf.SFREAD(SXRdiag, self.parent.shot, experiment="AUGD", edition=int(0))
                SXRtime = np.asarray(sfSXR.gettimebase(SXRsig,tbeg = self.parent.SXRtime[0], tend = self.parent.SXRtime[-1]))
                SXRdata = np.asarray(sfSXR.getobject(SXRsig, cal=True, tbeg = self.parent.SXRtime[0], tend = self.parent.SXRtime[-1]))

            if (SXR_choice == "FastSXR_I_054"):
                # SXRdiag = "SXG"
                SXRdiag = "SXF"
                SXRsig = "I_054"
                sfSXR = sf.SFREAD(SXRdiag, self.parent.shot, experiment="AUGD", edition=int(0))
                SXRtime = np.asarray(sfSXR.gettimebase(SXRsig, tbeg = self.parent.SXRtime[0], tend = self.parent.SXRtime[-1]))
                SXRdata = np.asarray(sfSXR.getobject(SXRsig, cal=True, tbeg = self.parent.SXRtime[0], tend = self.parent.SXRtime[-1]))


            ### ECE load
            ECEdata = self.parent.ECp.Te
            ECEtime = self.parent.ECp.time
            ECE_choice = int(self.chooseECE_ECESXR.currentText())
            idx_ECE_ch = list(self.parent.ECp.chs).index(ECE_choice) 

            # normalization
            SXRdata_norm = (SXRdata-np.mean(SXRdata))/np.max(SXRdata-np.mean(SXRdata))
            ECEdata_norm = (ECEdata[:,idx_ECE_ch] - np.mean(ECEdata[:,idx_ECE_ch]))/np.max(ECEdata[:,idx_ECE_ch] - np.mean(ECEdata[:,idx_ECE_ch]))

            # plot
            self._static_ax_ECESXR.clear()
            self._static_ax_ECESXR.plot(
                            SXRtime, SXRdata_norm, "k-", label="SXR")
            # in calculataion ("standard shot"): t_ECE_period in range [t_SXR - dt_shift - T ,t_SXR - dt_shift]
            # in preview to align ECE and SXR data ("standard shot"): t_ECEpreview = t_ECE + dt_shift
            self._static_ax_ECESXR.plot(
                            ECEtime+self.parent.dt_SXR_ECE, ECEdata_norm, "r-", label="ECE(ch: %g)"%(ECE_choice))
            self._static_ax_ECESXR.plot(
                            ECEtime, ECEdata_norm, "b-", label="ECE without dt_shift = %g s" %(self.parent.dt_SXR_ECE), alpha=0.10)
            self._static_ax_ECESXR.set_xlabel("t [s]")
            self._static_ax_ECESXR.set_ylabel("normalized Ampl")
            self._static_ax_ECESXR.grid()
            self._static_ax_ECESXR.legend(fontsize=10)
            self._static_ax_ECESXR.set_title("#%g" %(self.parent.shot))

            self.tline_B = ECEtime[0]
            self.tline_E = ECEtime[-1]
            self.lver_B_ECESXR = self._static_ax_ECESXR.axvline(x=self.tline_B, color="g")
            self.lver_E_ECESXR = self._static_ax_ECESXR.axvline(x=self.tline_E, color="r")
            click_coord = self.static_canvas_ECESXR.mpl_connect(
                'button_press_event', self.mouse_click_ECESXR)
            self.static_canvas_ECESXR.draw()


        except Exception as exc:
            # Handle the error gracefully
            tb = traceback.format_exc()
            print(f"An error occurred: {exc}\nTraceback:\n{tb}")

    def mouse_click_ECESXR(self, event):
        try:
            if (event.dblclick == True) & (event.button == 1):
                self.tline_B = event.xdata
                self.lver_B_ECESXR.set_xdata(self.tline_B)
                dt = self.tline_E - self.tline_B
                reverse_dt = 1.0*1e-3/dt
                self._static_ax_ECESXR.set_title("#%g, dt = %0.3g s, 1/dt = %0.3g kHz" %(self.parent.shot,dt,reverse_dt))
                self.static_canvas_ECESXR.draw()
            if (event.dblclick == True) & (event.button == 3):
                self.tline_E = event.xdata
                self.lver_E_ECESXR.set_xdata(self.tline_E)
                dt = self.tline_E - self.tline_B
                reverse_dt = 1.0*1e-3/dt
                self._static_ax_ECESXR.set_title("#%g, dt = %0.3g s, 1/dt = %0.3g kHz" %(self.parent.shot,dt,reverse_dt))
                self.static_canvas_ECESXR.draw()

        except Exception as exc:
            # Handle the error gracefully
            tb = traceback.format_exc()
            print(f"An error occurred: {exc}\nTraceback:\n{tb}")

    def reload_new_fmode(self):
        "reload ECE data with new f_(m,n) falue"
        try:
            self.parent.mode_f_lineEdit.setText(self.lineEdit_fmn_ECESXR.text()) 
            self.parent.load_data_ECE()
            self.plot_func_ECESXR()

        except Exception as exc:
            # Handle the error gracefully
            tb = traceback.format_exc()
            print(f"An error occurred: {exc}\nTraceback:\n{tb}")
        
            

    def SaveValues(self, init=False):

        """
        Save values from Data Settings dialog
        """
        """
        self.setting['shot'] = int(self.parent.lineEdit_Shot.text())
        self.parent.tmin = self.tmin_spin.value()
        self.setting['tmin'] = self.parent.tmin
        self.parent.tmax = self.tmax_spin.value()
        self.setting['tmax'] = self.parent.tmax
        self.parent.data_smooth = self.smoothSpin.value()
        self.setting['data_smooth'] = self.parent.data_smooth
        self.parent.data_undersampling = self.UndersamplingSpin.value()
        self.setting['data_undersampling'] = self.parent.data_undersampling

            
        wrong_dets_pref =  self.ReadWrongDets(init)
        wrong = self.tokamak.dets[~self.tokamak.get_correct_dets(include_pref  = False)]
        config.wrong_dets_pref  = unique(setdiff1d( wrong_dets_pref,wrong))
        
        
                    
        if not os.path.exists(self.tokamak.geometry_path+'/wrong_channels/'):
            os.mkdir(self.tokamak.geometry_path+'/wrong_channels/')

        savetxt(self.tokamak.geometry_path+'/wrong_channels/%d'%self.setting['shot'], config.wrong_dets_pref,fmt='%d')

        self.parent.setting = self.setting
        self.parent.tokamak = self.tokamak
        """
        pass

    def RefreshEvent(self, init=False):
        """
        Reload preview when Refresh button pressed
        """
        # self.tmin = double(self.tmin_lineEdit.text())
        # self.tmax = double(self.tmax_lineEdit.text())
        
        remove_chs = self.ReadWrongDets()
        self.SetWrongDets(remove_chs)
        self.parent.getValues()
        self.parent.load_data_ECE()
        self.parent.ECp.remove_chs_NN(remove_chs)
        # self.parent.ECp.cut_data(self.tmin, self.tmax)
        self.plot_main()

    def CloseEvent(self, *event):
        # self.SaveValues()
        self.close()
    
                
    def setCenter(self):
        frect = QDesktopWidget.frameGeometry(self)
        frect.moveCenter(QDesktopWidget().availableGeometry(self).center());
        self.move(frect.topLeft())
        
        
    def ReadWrongDets(self,init=False):
        dets = str(self.Edit_wrongDetectors.text())
        if len(dets.strip()) > 0:
            try:
                # [osam]: include the end point in slices
                mylist = dets.split(",")
                wrong_dets_pref = array([]).astype(int)
                for el in mylist:
                    if ":" in el:
                        tempchs = eval('r_['+el+']')
                        tempchs = array(int_(tempchs), ndmin=1)
                        tempchs = append(tempchs, tempchs[-1]+int(1))
                    else:
                        tempchs = eval('r_['+el+']')
                        tempchs = array(int_(tempchs), ndmin=1)
                    wrong_dets_pref = append(wrong_dets_pref, tempchs)
            except:
                wrong_dets_pref = []
                QMessageBox.warning(self,"Input problem",
                    "Wrong detectors are in bad format, use ...,10,11,13:15,...",QMessageBox.Ok)
                raise
        elif init:     # do not remove on init
            wrong_dets_pref = config.wrong_dets_pref
        else:
            wrong_dets_pref = []
        return  wrong_dets_pref
    
    def SetWrongDets(self, wrong):
        
        wrong = unique(wrong)
        print("SetWrongDets, wrong:")
        print(wrong)

        wrong_dets_str = ''
        i = 0
        while i < len(wrong):
            # [osam]: remove the +1 shift in the beginning, numbering as in background python dets dictionary
            # wrong_dets_str = wrong_dets_str+str(wrong[i]+1)
            wrong_dets_str = wrong_dets_str+str(wrong[i])
            if i+1 < len(wrong) and wrong[i+1] == wrong[i]+1:
                while i+1 < len(wrong) and wrong[i+1] == wrong[i]+1:
                    i += 1
                # [osam]: change the +2 shift to match numbering as in background python dets dictionary, plus including the end point in slices
                # wrong_dets_str = wrong_dets_str+':'+str(wrong[i]+2)
                wrong_dets_str = wrong_dets_str+':'+str(wrong[i])
            i += 1
            wrong_dets_str = wrong_dets_str+','
        wrong_dets_str = wrong_dets_str[:-1]
            
        self.Edit_wrongDetectors.setText(wrong_dets_str)



