import time
import numpy as np
import os
from scipy import interpolate
from modulesECE.func_brems_emiss_calc import calc_brems
from multiprocessing import Process, Pool, cpu_count

global minTe
minTe = 100.0 # [eV]

def Brems_calc(received_data):
    # calc func, that receives data and initiate multiprocessing
    timespentplotting = time.time()
    (
        Te2Dfilt_arr,
        r_SXR,
        z_SXR,
        NN_2Dprofiles,
        ne,
        Zeff,
        max_Te
    ) = received_data
    
    try:
        n_cpu = cpu_count()
        p = Pool(n_cpu)
    except:
        n_cpu = 1

    print("Calculating Bremsstrahlung emission 2D profiles...")
    print(f"n_cpu = {n_cpu}")
    print(f"NN_2Dprofiles = {NN_2Dprofiles}")

    Te_for_interp = np.linspace(minTe, max_Te, 100) 
    emBr_for_interp = np.zeros_like(Te_for_interp)
    
    for j_T in range(len(Te_for_interp)):
        emBr_for_interp[j_T] = calc_brems(Te_for_interp[j_T], Zeff, ne) # W/m3
    
    # n_split = 1 if NN_2Dprofiles < 2*n_cpu else 2*n_cpu
    # n_split = 1 if NN_2Dprofiles < n_cpu else n_cpu
    n_split = n_cpu
    ind = [
        slice(i * NN_2Dprofiles // n_split, (i + 1) * NN_2Dprofiles // n_split)
        for i in range(n_split)
    ]
    NNjobs = np.arange(0,NN_2Dprofiles)
    # print(ind)
    args = [
        (
            Te2Dfilt_arr[ii],
            r_SXR,
            z_SXR,
            NN_2Dprofiles,
            ne,
            Zeff,
            NNjobs[ii],
            Te_for_interp,
            emBr_for_interp
        )
        for ii in ind
    ]
    
    results_multiproc = p.imap(calc_func,args,1)

    try:
        p.close()
        p.join()
    except:
        pass
    
    # Create variables for the calculated results
    emBr2D_arr = []

    # Loop through the list of results and append each value to the appropriate list
    for result in results_multiproc:
        emBr2D_arr.append(result[0])

    emBr2D_arr = [x for xs in emBr2D_arr for x in xs]
    
    # print(f"t_2Dprofiles = {t_2Dprofiles}")
    # print(f"Te2D_arr = {Te2D_arr}")
    # print(f"R2D_arr.shape = {R2D_arr.shape}")
    # R2D_arr = np.concatenate(R2D_arr)

    return emBr2D_arr


def calc_func(args):
    # print("calc initiated")
    T = time.time()
    os.nice(3)
    (
        Te2Dfilt_arr,
        r_SXR,
        z_SXR,
        NN_2Dprofiles,
        ne,
        Zeff,
        NNjobs,
        Te_for_interp,
        emBr_for_interp
    ) = args
    
    f_brem = interpolate.interp1d(Te_for_interp, emBr_for_interp, fill_value = "extrapolate")
    
    emBr2D_arr = []
    NN_z, NN_r =  len(z_SXR), len(r_SXR)
    for i_t, tstep in enumerate(NNjobs):
        emissivityBr = np.zeros_like(Te2Dfilt_arr[i_t])
        for i_z in range(NN_z):
            for i_r in range(NN_r):
                if (Te2Dfilt_arr[i_t][i_z,i_r]>=minTe):
                    emissivityBr[i_z,i_r] = f_brem(Te2Dfilt_arr[i_t][i_z,i_r])
                else:
                    emissivityBr[i_z,i_r] = 0.0
        emBr2D_arr.append(emissivityBr)
        print(f"Calculating Bremsstrahlung emission: {tstep+1}/{NN_2Dprofiles}")
    
    output_multiproc = [
    emBr2D_arr
    ]

    return output_multiproc
