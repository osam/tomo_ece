"""
to use these functions inside other python programs, paste the following:
import sys
sys.path.append("/afs/ipp-garching.mpg.de/home/o/osam/PythonLibraries")
import ECE_prepare as ECprep
import importlib
importlib.reload(ECprep)
then use as
ECp = ECprep.ECprep(time, Te, R, z, rhop, channels)
"""
import numpy as np
import collections
import aug_sfutils as sf
from scipy import interpolate
import time

class ECprep:
    def __init__(self, eqm, shot, time, Te, R, z, rhop, channels):
        self.eqm = eqm
        self.shot = shot
        self.chs_all = channels
        self.time_full = time
        self.Te_full = Te
        self.R_full = R
        self.z_full = z
        self.rhop_full = rhop
        # below the data is doubled for later cropping/cutting
        self.chs = channels
        self.time = time
        self.Te = Te
        self.R = R
        self.z = z
        self.rhop = rhop
        print("Class ECprep is initiated")

    def cut_data(self, tmin, tmax):
        idxB = self.find_nearest_idx(self.time_full, tmin)
        idxE = self.find_nearest_idx(self.time_full, tmax)
        
        if hasattr(self, 'idxs_use'):
            self.Te = self.Te_full[idxB:idxE, self.idxs_use]
            self.R = self.R_full[idxB:idxE, self.idxs_use]
            self.z = self.z_full[idxB:idxE, self.idxs_use]
            self.rhop = self.rhop_full[idxB:idxE, self.idxs_use]
        else:
            self.Te = self.Te_full[idxB:idxE, :]
            self.R = self.R_full[idxB:idxE, :]
            self.z = self.z_full[idxB:idxE, :]
            self.rhop = self.rhop_full[idxB:idxE, :]
        self.time = self.time_full[idxB:idxE]
        print("Data is cut within the range t = %g - %g s " %(
            self.time[0], self.time[-1]))

    def remove_chs_NN(self, chs_to_del):
        "remove given channels"
        self.idxs_del = self.find_idxs_of_el_in_arr(self.chs_all, chs_to_del)
        Array_temp = np.arange(len(self.chs))
        self.idxs_use = np.delete(Array_temp, self.idxs_del)
        self.Te = self.Te_full[:, self.idxs_use]
        self.R = self.R_full[:, self.idxs_use]
        self.z = self.z_full[:, self.idxs_use]
        self.rhop = self.rhop_full[:, self.idxs_use]
        self.chs = self.chs_all[self.idxs_use]
        
    def find_idxs_of_el_in_arr(self, array, elements):
        "find indexes of elements in an array"
        idxs = []
        for i in elements:
            if i in array:
                idx_i = list(array).index(i)
                idxs.append(idx_i)
        return idxs

    def fft_analysis(self, t, y):
        " returns freq and normalized(fft(y)) and offset "
        " apply Y[range(s_length/2)] "
        Ts = t[1] - t[0] # sampling interval
        Fs = 1.0/Ts # sampling rate
        s_length = len(t) # length of the signal
        k = np.arange(s_length) # coef for calc freq
        T = s_length/Fs # coef for calc freq
        frq = k/T # two sides frequency range
        # frq =frq[range(s_length/2)] # one side frequency range 
        offset = np.mean(y)
        y_ed = y - offset
        Y = np.fft.fft(y_ed)/s_length # fft computing and normalization
        NN = len(frq)
        frq = frq[:int(NN / 2)]
        Y = Y[:int(NN / 2)]
        # Y = Y[range(s_length/2)]
        return frq, Y, offset

    def getDominantFreq_FFT(self):
        """
        Find freq of a max in FFT spectra. 
        """
        dom_freq_arr = []
        for i_ch, elem in enumerate(self.chs):
            signal = self.Te[:, i_ch] - np.mean(self.Te[:, i_ch]) 
            C = np.correlate(signal, signal, 'same') # autocorrelate to reduce noise
            frq, FFT_data, offset = self.fft_analysis(self.time, C)
            FFT_data = np.abs(FFT_data)
            dominant_frequency = frq[np.argmax(FFT_data)]
            dom_freq_arr.append(dominant_frequency)
        self.dom_f_arr_FFT = dom_freq_arr
        self.dom_f_FFT = collections.Counter(dom_freq_arr).most_common(1)[0][0]
    
    def getDominantFreq_PtP(self):
        " Find dominant freq of a signal using finding peak approach. "
        from scipy.signal import find_peaks
        dom_freq_arr = []
        for i_ch, elem in enumerate(self.chs):
            signal = self.Te[:, i_ch] - np.mean(self.Te[:, i_ch]) 
            C = np.correlate(signal, signal, 'same') # autocorrelate to reduce noise
            peaks, _ = find_peaks(C, height=0)
            T_avg = np.average(np.diff(self.time[peaks]))
            dominant_frequency = 1.0/T_avg
            dom_freq_arr.append(dominant_frequency)
        self.dom_f_arr_PtP = dom_freq_arr
        self.dom_f_PtP = collections.Counter(dom_freq_arr).most_common(1)[0][0]

    def find_nearest_idx(self, array, value):
        "find nearest idx in array for the value"
        idx = (np.abs(array - value)).argmin()
        return idx

    def cut_data_mode_period(self, t_ECErec, f_mode, m):
        T = np.abs(m*(1.0/f_mode))
        tB = t_ECErec - T
        tE = t_ECErec
        idxB = self.find_nearest_idx(self.time_full, tB)
        idxE = self.find_nearest_idx(self.time_full, tE)
        self.time_T = self.time_full[idxB:idxE]
        self.Te_T = self.Te_full[idxB:idxE, :]
        self.R_T = self.R_full[idxB:idxE, :]
        self.z_T = self.z_full[idxB:idxE, :]
        self.rhop_T = self.rhop_full[idxB:idxE, :]
        print("Data is cut for one mode period T = %g micro s (f_mode = %g kHz, m = %g)" % (
            T*1e6, f_mode*1e-3, m))


    def find_closest_indices_btw_arrays(self, arr_search, arr_find_in):
        " Find the index of the closest value in arr_find_in to each value in array_search."
        closest_indices = np.abs(arr_find_in - arr_search[:, np.newaxis]).argmin(axis=1)
        return closest_indices

    def find_closest_indices_btw_arraysV2(self, arr_search, arr_find_in):
        " Find the index of the closest value in arr_find_in to each value in array_search. Faster than the version above"
        # Interpolate B at the positions of A and round the results to the nearest integer
        closest_indices = np.round(np.interp(arr_search, arr_find_in, np.arange(len(arr_find_in)))).astype(int)
        return closest_indices

    def map_to_thetaStar(self, theta_star0_chs):
        "theta_star0_chs is default theta_star of ECE channels"
        # eqm = sf.EQU(self.shot, diag=equ_diag)
        T0 = time.time()
        rho_my = np.mean(self.rhop_T, axis=0)
        Nrho_my = len(rho_my)
        Ntime_my = len(self.time_T)
        Nrho_sf = 200
        # Ntheta_sf = 10*Ntime_my
        Ntheta_sf = 2*Ntime_my
        # same definition as in sf.mag_theta_star
        rho_sf = np.linspace(0, 1, Nrho_sf+1)[1:]
        print("MARKER 1, time:%2.2f" %(time.time()-T0))
        R, z, theta_star = sf.mag_theta_star(self.eqm, t_in=np.mean(
            self.time_T), n_rho=Nrho_sf, n_theta=Ntheta_sf, rz_grid=False)
        print("MARKER 2, time:%2.2f" %(time.time()-T0))
        indexes_rho = self.find_closest_indices_btw_arraysV2(rho_my, rho_sf)

        print("MARKER 3, time:%2.2f" %(time.time()-T0))
        self.R_thetaS = np.zeros_like(self.rhop_T)
        self.z_thetaS = np.zeros_like(self.rhop_T)
        self.theta_star = np.zeros([Ntime_my, Nrho_my])
        print(f"Nrho_my = {Nrho_my}")
        for i_ch in range(Nrho_my):
            theta_star_my = np.linspace(
                0.0+theta_star0_chs[i_ch], 2*np.pi+theta_star0_chs[i_ch], Ntime_my, endpoint=True)
            theta_star_my = self.wrap_to_2pi(theta_star_my)
            print("MARKER 3.1, time:%2.2f" %(time.time()-T0))
            inds_thetaStar = self.find_closest_indices_btw_arraysV2(
                theta_star_my, theta_star[indexes_rho[i_ch], :])
            print("MARKER 3.2, time:%2.2f" %(time.time()-T0))
            self.R_thetaS[:, i_ch] = R[indexes_rho[i_ch], inds_thetaStar]
            self.z_thetaS[:, i_ch] = z[indexes_rho[i_ch], inds_thetaStar]
            self.theta_star[:, i_ch] = theta_star_my
            
        print("MARKER 4, time:%2.2f" %(time.time()-T0))

    def wrap_to_2pi(self, angle):
        """
        Wrap an angle to the range 0 to 2pi radians.
        """
        return angle % (2*np.pi)

    def interp_on_timescale(self, time_interp_on):
        " interpolate on a timescale with higher temporal resolution"
        print("Interpolating (linear) Te, rhop, R, z on the fastSXR time range")
        print("Temporal resolution changed from %g micro s to %g micro s" % (
            (self.time[1]-self.time[0])*1e6, (time_interp_on[1]-time_interp_on[0])*1e6))
        NN_new_time = len(time_interp_on)
        NN_chs = self.Te.shape[1] 
        Te_int, R_int, z_int, rhop_int = np.zeros((NN_new_time, NN_chs)), np.zeros(
            (NN_new_time, NN_chs)), np.zeros((NN_new_time, NN_chs)), np.zeros((NN_new_time, NN_chs))
        for i_ch in range(NN_chs):
            print(f"shape time full = {self.time_full.shape}")
            print(f"shape Te.Te[:, i_ch] = {self.Te[:, i_ch].shape}")
            f_Te = interpolate.interp1d(
                self.time_full, self.Te[:, i_ch], kind='linear', bounds_error=False, fill_value="extrapolate")
            f_R = interpolate.interp1d(
                self.time_full, self.R[:, i_ch], kind='linear', bounds_error=False, fill_value="extrapolate")
            f_z = interpolate.interp1d(
                self.time_full, self.z[:, i_ch], kind='linear', bounds_error=False, fill_value="extrapolate")
            f_rhop = interpolate.interp1d(
                self.time_full, self.rhop[:, i_ch], kind='linear', bounds_error=False, fill_value="extrapolate")
            Te_int[:, i_ch] = f_Te(time_interp_on)
            R_int[:, i_ch] = f_R(time_interp_on)
            z_int[:, i_ch] = f_z(time_interp_on)
            rhop_int[:, i_ch] = f_rhop(time_interp_on)

        self.time_full = time_interp_on
        self.Te_full = Te_int
        self.R_full = R_int
        self.z_full = z_int
        self.rhop_full = rhop_int


