import time
import numpy as np
import os
import aug_sfutils as sf
from scipy import interpolate
from scipy.interpolate import griddata # interpolation
from scipy.ndimage import gaussian_filter # 2D filter gaussian


# r_int, z_int = np.meshgrid(r_SXR, z_SXR)
# Te_int = griddata((R2D_arr[i].flatten(), z2D_arr[i].flatten()), Te2D_arr[i].flatten(), (r_int, z_int), method='linear', fill_value = 0.0)
# Te_int[Te_int < 0.0] = 0.0
# Te_filt = gaussian_filter(Te_int,sigmaGauss,mode="nearest")

try:
    from multiprocessing import Process, Pool, cpu_count
    import threading
    threading._DummyThread._Thread__stop = lambda x:40
except:
    pass


def prECEdata_multipr(received_data):
    # plotting func, that receives data and initiate multiprocessing
    timespentplotting = time.time()
    (
        t_T_arr,
        rhop_T_arr,
        Te_T_arr,
        eqm,
        t_2Dprofiles,
        NN_2Dprofiles,
        theta_star_LFS,
        r_SXR,
        z_SXR,
        sigmaGauss
    ) = received_data
    
    r_int, z_int = np.meshgrid(r_SXR, z_SXR)
    
    try:
        n_cpu = cpu_count()
        p = Pool(n_cpu)
    except:
        n_cpu = 1

    print("Restructuring ECE data for plotting ...")
    print(f"n_cpu = {n_cpu}")
    print(f"NN_2Dprofiles = {NN_2Dprofiles}")

    # n_split = 1 if NN_2Dprofiles < 2*n_cpu else 2*n_cpu
    n_split = 1 if NN_2Dprofiles < n_cpu else n_cpu
    ind = [
        slice(i * NN_2Dprofiles // n_split, (i + 1) * NN_2Dprofiles // n_split)
        for i in range(n_split)
    ]
    NNjobs = np.arange(0,NN_2Dprofiles)
    print(ind)
    args = [
        (
            t_T_arr[ii],
            rhop_T_arr[ii],
            Te_T_arr[ii],
            eqm,
            t_2Dprofiles[ii],
            NNjobs[ii],
            NN_2Dprofiles,
            theta_star_LFS,
            r_int,
            z_int,
            sigmaGauss
        )
        for ii in ind
    ]
    
    results_multiproc = p.imap(calc_func,args,1)

    try:
        p.close()
        p.join()
    except:
        pass
    
    # Create variables for the calculated results
    t_2Dprofiles, t_T_arr = [], []
    R2D_arr, z2D_arr, Te2D_arr, thetaS_arr = [], [], [], []
    Te2Dfilt_arr = []

    # Loop through the list of results and append each value to the appropriate list
    for result in results_multiproc:
        t_2Dprofiles.append(result[0])
        t_T_arr.append(result[1])
        Te2D_arr.append(result[2])
        R2D_arr.append(result[3])
        z2D_arr.append(result[4])
        thetaS_arr.append(result[5])
        Te2Dfilt_arr.append(result[6])

    t_2Dprofiles = np.concatenate(t_2Dprofiles)
    t_T_arr = np.concatenate(t_T_arr)
    Te2D_arr = [x for xs in Te2D_arr for x in xs]
    R2D_arr = [x for xs in R2D_arr for x in xs]
    z2D_arr = [x for xs in z2D_arr for x in xs]
    thetaS_arr = [x for xs in thetaS_arr for x in xs]
    Te2Dfilt_arr = [x for xs in Te2Dfilt_arr for x in xs]
    
    # print(f"t_2Dprofiles = {t_2Dprofiles}")
    # print(f"Te2D_arr = {Te2D_arr}")
    # print(f"R2D_arr.shape = {R2D_arr.shape}")
    # R2D_arr = np.concatenate(R2D_arr)

    return t_2Dprofiles, t_T_arr, Te2D_arr, R2D_arr, z2D_arr, thetaS_arr, Te2Dfilt_arr


def calc_func(args):
    # print("calc initiated")
    T = time.time()
    os.nice(3)
    (
        t_T_arr,
        rhop_T_arr,
        Te_T_arr,
        eqm,
        t_2Dprofiles,
        NNjobs,
        NN_2Dprofiles,
        theta_star0_chs,
        r_int,
        z_int,
        sigmaGauss
    ) = args
    
    
    
    R2D_arr, z2D_arr, Te2D_arr, thetaS_arr, Te2Dfilt_arr = [], [], [], [], []
    for i, tstep in enumerate(NNjobs):
        rho_my = np.mean(rhop_T_arr[i], axis=0)
        Nrho_my = len(rho_my)
        Ntime_my = len(t_T_arr[i])
        Nrho_sf = 200
        # Ntheta_sf = 10*Ntime_my
        Ntheta_sf = 2*Ntime_my
        # same definition as in sf.mag_theta_star
        rho_sf = np.linspace(0, 1, Nrho_sf+1)[1:]
        R, z, theta_star = sf.mag_theta_star(
            eqm, t_in=np.mean(t_T_arr[i]),
            n_rho=Nrho_sf, n_theta=Ntheta_sf,
            rz_grid=False
        )
        rho_my = rho_my.astype(float)
        indexes_rho = find_closest_indices_btw_arrays(
                rho_my, rho_sf)
        
        R_thetaS = np.zeros_like(rhop_T_arr[i])
        z_thetaS = np.zeros_like(rhop_T_arr[i])
        thetaS = np.zeros([Ntime_my, Nrho_my])
        for i_ch in range(Nrho_my):
            theta_star_my = np.linspace(
                0.0+theta_star0_chs[i_ch],
                2*np.pi+theta_star0_chs[i_ch],
                Ntime_my, endpoint=True)
            theta_star_my = wrap_to_2pi(theta_star_my)
            inds_thetaStar = find_closest_indices_btw_arrays(
                theta_star_my, theta_star[indexes_rho[i_ch], :])
            R_thetaS[:, i_ch] = R[indexes_rho[i_ch], inds_thetaStar]
            z_thetaS[:, i_ch] = z[indexes_rho[i_ch], inds_thetaStar]
            thetaS[:, i_ch] = theta_star_my
        
        # interpolating on the SXR grid
        Te_int = griddata((R_thetaS.flatten(), z_thetaS.flatten()), Te_T_arr[i].flatten(), (r_int, z_int), method='linear', fill_value = 0.0)
        Te_int[Te_int < 0.0] = 0.0
        
        # applying the 2D Gaussian filter
        Te_filt = gaussian_filter(Te_int,sigmaGauss,mode="nearest")
            
        R2D_arr.append(R_thetaS)
        z2D_arr.append(z_thetaS)
        Te2D_arr.append(Te_T_arr[i])
        thetaS_arr.append(thetaS)
        Te2Dfilt_arr.append(Te_filt)
        
        print(f"Preparing 2D ECE profiles: {tstep+1}/{NN_2Dprofiles}")
        # print("Prepare data: %g/%g" %(i+1, NNjobs))
    
    output_multiproc = [
    t_2Dprofiles,
    t_T_arr,
    Te2D_arr,
    R2D_arr,
    z2D_arr,
    thetaS_arr,
    Te2Dfilt_arr
    ]

    return output_multiproc



def wrap_to_2pi(angle):
    """
    Wrap an angle to the range 0 to 2pi radians.
    """
    return angle % (2*np.pi)


def find_closest_indices_btw_arrays(arr_search, arr_find_in):
    " Find the index of the closest value in arr_find_in to each value in array_search. Faster than the version above"
    # Interpolate B at the positions of A and round the results to the nearest integer
    closest_indices = np.round(np.interp(arr_search, arr_find_in, np.arange(len(arr_find_in)))).astype(int)
    return closest_indices

