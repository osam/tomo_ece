import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("gffgu_mod.dat", skiprows=5)
u_raw = data[0:3321,0]
g_raw = data[0:3321,1]
u = np.sort(np.unique(u_raw)) # len 41
g = np.sort(np.unique(g_raw)) # len 81

gff_orig = data[0:3321,2]
gaunt_factor = gff_orig.reshape((len(u),len(g)))
gamma_grid = np.log10(u)
u_grid = np.log10(g)

# gff 

# data = pd.read_csv("gffgu_mod.dat",sep=" ", skiprows=5)

np.savez('u_gamma_gauntfactor.npz', u=u_grid, gamma=gamma_grid, gaunt_factor=gaunt_factor)
