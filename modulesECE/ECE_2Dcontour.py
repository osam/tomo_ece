"""
to use these functions inside other python programs, paste the following:
import sys
sys.path.append("/afs/ipp-garching.mpg.de/home/o/osam/PythonLibraries")
import ECE_2Dcontour as ECEcontour
import importlib
importlib.reload(ECEcontour)
then use as
EC=ECE.ECE()
EC.Load(Shot, Diagnostic='CEC')
"""
import numpy as np
import matplotlib.pyplot as plt


class ECEcontour:
    def __init__(self, time, rr, Te, channels):
        self.time = time
        self.rr = rr
        self.Te = Te
        self.chs = channels
        self.filter_applied = False
        self.fs = len(self.time) / \
            (self.time[-1] - self.time[0])  # Sample rate (Hz)
        print("Class ECEcontour is initiated.")
        print("Sampling rate of the signal: %g kHz" % (self.fs*1e-3))

    def plot(self, contourNN):
        "plot 2D contour from ECE"
        if self.filter_applied == True:
            Te_plot = self.Te_filt
        else:
            Te_plot = self.Te
        time_plot = np.zeros_like(self.Te)
        for i in range(self.rr.shape[1]):
            time_plot[:,i] = self.time
        rr_plot = self.rr

        fig, ax = plt.subplots(figsize=(18, 10))
        contours = ax.contourf(time_plot, rr_plot, Te_plot, levels = contourNN, cmap = "jet")
        ax.contour(time_plot, rr_plot, Te_plot, levels = contourNN, cmap = "binary")

        cbar = fig.colorbar(contours, ax=ax)
        cbar.ax.tick_params(labelsize=8, rotation=90) 
        cbar.ax.set_ylabel('Trad [eV]', rotation=90)

        ax.set_xlabel("time [s]")
        ax.set_ylabel("R [m] (or rhop)")
        t_chs_plot = time_plot[0]*np.ones(rr_plot.shape[1])
        ax.plot(t_chs_plot, rr_plot[0,:], "ko")

        for i, txt in enumerate(self.chs):
            ax.annotate(txt, (t_chs_plot[i], rr_plot[0,i]),fontsize=10)

        plt.show()

    def Apply_SavGol(self, win_len=30, polyorder=3):
        from scipy import signal
        self.Te_filt = np.zeros_like(self.Te)
        for i_ch in range(self.Te.shape[1]):
            self.Te_filt[:, i_ch] = signal.savgol_filter(
                self.Te[:, i_ch], win_len, polyorder)
        self.filter_applied = True
        print("SavGol filter applied: win_len = %g, polyn. ord. = %g" %
              (win_len, polyorder))

    def Apply_ButterBand(self, f_low=0.0, f_high=1.0e4, order=4):
        from scipy import signal
        # Normalized cutoff frequencies:
        Wn = [f_low / (0.5 * self.fs), f_high / (0.5 * self.fs)]
        b, a = signal.butter(order, Wn, 'band')
        self.Te_filt = np.zeros_like(self.Te)
        for i_ch in range(self.Te.shape[1]):
            self.Te_filt[:, i_ch] = signal.filtfilt(b, a, self.Te[:, i_ch])
        self.filter_applied = True
        print("Butterworth band-pass filter applied: f_low = %g kHz, f_high = %g kHz" %
              (f_low*1e-3, f_high*1e-3))

    def Apply_ButterLowPass(self, f_cut=1.0e4, order=4):
        from scipy import signal
        # Normalized cutoff frequencies:
        b, a = signal.butter(order, f_cut / (0.5 * self.fs), btype='low')
        self.Te_filt = np.zeros_like(self.Te)
        for i_ch in range(self.Te.shape[1]):
            self.Te_filt[:, i_ch] = signal.filtfilt(b, a, self.Te[:, i_ch])
        self.filter_applied = True
        print("Butterworth band-pass filter applied: f_cut = %g kHz" %
              (f_cut*1e-3))


    def Apply_FFTBand(self, f_low=0.0, f_high=1.0e4):
        pass