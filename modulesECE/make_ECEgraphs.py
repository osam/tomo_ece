import time
import numpy as np
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvas as FigureCanvas
import os
import matplotlib
import matplotlib.pyplot as plt
# from scipy.interpolate import griddata # interpolation
# from scipy.ndimage import gaussian_filter # 2D filter gaussian
from matplotlib.colors import Normalize  
from matplotlib import colors # to define personal cmap
from matplotlib.ticker import MaxNLocator # for colorbar ticks
from matplotlib.transforms import Bbox # for saving figure

try:
    from multiprocessing import Process, Pool, cpu_count
    import threading
    threading._DummyThread._Thread__stop = lambda x:40
except:
    pass


def make_ECEgraphs(input_data):
    # plotting func, that receives data and initiate multiprocessing
    timespentplotting = time.time()
    (
        R2D_arr,
        z2D_arr,
        Te2D_arr,
        tTrace_arr,
        rrTrace_arr,
        TeTrace_arr,
        NN_2Dprofiles,
        time_2Dprofiles,
        ECE_SXR_plotmap_idxs,
        path_tmp,
        Shot,
        r_SXR,
        z_SXR,
        emiss_SXR,
        time_SXR,
        sigmaGauss,
        Te2Dfilt_arr,
        max_Te,
        max_emiss_SXR,
        SXRnormperturb_check,
        perturb_emiss_norm,
        max_perturb_emiss,
        min_perturb_emiss,
        emBr2D_arr,
        max_emBr,
    ) = (
        input_data["R2D_arr"],
        input_data["z2D_arr"],
        input_data["Te2D_arr"],
        input_data["tTrace_arr"],
        input_data["rrTrace_arr"],
        input_data["TeTrace_arr"],
        input_data["NN_2Dprofiles"],
        input_data["time_2Dprofiles"],
        input_data["ECE_SXR_plotmap_idxs"],
        input_data["path_tmp"],
        input_data["Shot"],
        input_data["r_SXR"],
        input_data["z_SXR"],
        input_data["emiss_SXR"],
        input_data["time_SXR"],
        input_data["sigmaGauss"],
        input_data["Te2Dfilt_arr"],
        input_data["max_Te"],
        input_data["max_emiss_SXR"],
        input_data["SXRnormperturb_check"],
        input_data["perturb_emiss_norm"],
        input_data["max_perturb_emiss"],
        input_data["min_perturb_emiss"],
        input_data["emBr2D_arr"],
        input_data["max_emBr"]
    )

    try:
        n_cpu = cpu_count()
        p = Pool(n_cpu)
    except:
        n_cpu = 1

    print(f"n_cpu = {n_cpu}")
    print(f"NN_2Dprofiles = {NN_2Dprofiles}")

    # n_split = 1 if NN_2Dprofiles < 2*n_cpu else 2*n_cpu
    # n_split = 1 if NN_2Dprofiles < n_cpu else n_cpu
    n_split = NN_2Dprofiles if NN_2Dprofiles < n_cpu else n_cpu
    ind = [
        slice(i * NN_2Dprofiles // n_split, (i + 1) * NN_2Dprofiles // n_split)
        for i in range(n_split)
    ]
    NNjobs = np.arange(0,NN_2Dprofiles)
    print(ind)
    # print(f"ECE_SXR_plotmap_idxs.shape = {ECE_SXR_plotmap_idxs.shape} ")
    
    args = [
        (
            R2D_arr,
            z2D_arr,
            Te2D_arr,
            tTrace_arr,
            rrTrace_arr,
            TeTrace_arr,
            time_2Dprofiles,
            ECE_SXR_plotmap_idxs[ii],
            NNjobs[ii],
            path_tmp,
            Shot,
            r_SXR,
            z_SXR,
            emiss_SXR[:,:,ii],
            time_SXR[ii],
            sigmaGauss,
            Te2Dfilt_arr,
            max_Te,
            max_emiss_SXR,
            SXRnormperturb_check,
            perturb_emiss_norm[:,:,ii],
            max_perturb_emiss,
            min_perturb_emiss,
            emBr2D_arr,
            max_emBr,
            NN_2Dprofiles
        )
        for ii in ind
    ]

    p.imap(plotfunc,args,1)

    try:
        p.close()
        p.join()
    except:
        pass
    
    
    print('\rPlotting time %.1f s' % (time.time()-timespentplotting))

def plotfunc(args):
    # print("plotfunc initiated")
    T = time.time()
    os.nice(3)
    (
        R2D_arr,
        z2D_arr,
        Te2D_arr,
        tTrace_arr,
        rrTrace_arr,
        TeTrace_arr,
        time_2Dprofiles,
        ECE_SXR_plotmap_idxs,
        NNjobs,
        path_tmp,
        Shot,
        r_SXR,
        z_SXR,
        emiss_SXR,
        time_SXR,
        sigmaGauss,
        Te2Dfilt_arr,
        max_Te,
        max_emiss_SXR,
        SXRnormperturb_check,
        perturb_emiss_norm,
        max_perturb_emiss,
        min_perturb_emiss,
        emBr2D_arr,
        max_emBr,
        NN_2Dprofiles
    ) = args
    img_size = 6
    dpi = 80
    contourNN = 20
    extend = 'min' # coloring of values that are outside the levels range 
    # xmin, xmax = 1.28, 2.05
    xmin, xmax = 1.20, 2.10
    ymin, ymax = -0.5, 0.6
    vminTe, vmaxTe = 0.0, max_Te # [eV]
    vminSXR, vmaxSXR = 0.0, max_emiss_SXR*1e-3 # [kW/m3]
    vminBr, vmaxBr = 0.0, max_emBr*1e-3 # [kW/m3]
    vminW, vmaxW = 0.0, max(max_emBr*1e-3,max_emiss_SXR*1e-3) # [kW/m3]
    vminSXR_prtb, vmaxSXR_prtb = min_perturb_emiss*1e-3, max_perturb_emiss*1e-3 # [kW/m3]
    
    cdict = {
    'red': ((0, 0.1, 0.1),(0.04, 0.15, 0.15),(0.07, 0.25, 0.25), (0.12, 0.4, 0.4),\
        (0.2, 0.6, 0.6),(0.35, 0.9, 0.9),(0.4, 0.9, 0.9),(0.6, 1, 1),(0.8, 1, 1),(0.9, 1, 1),(1, 1, 1)),
    'green': ((0, 0.1, 0.1),(0.04, 0.15, 0.15),(0.07, 0.2, 0.2), (0.12, 0.15,0.15 ),\
        (0.2, 0.1, 0.1),(0.35, 0.1, 0.1),(0.4, 0.1, 0.1),(0.6, 0.6, 0.6),(0.8, 1, 1),(0.9, 1, 1),(1, 1, 1)),
    'blue' : ((0, 0.3, 0.3),(0.04, 0.5, 0.5),(0.07, 0.6, 0.6), (0.12, 0.6,0.6),\
        (0.2, 0.6, 0.6),(0.35, 0.6, 0.6),(0.4, 0.1, 0.1), (0.6, 0, 0),(0.8, 0, 0),(0.9, 0.8, 0.8),(1, 1, 1))
    }
    my_cmap = colors.LinearSegmentedColormap('my_colormap', cdict, 1024)
    
    # print(f"NNjobs={NNjobs}")
    # print(f"ECE_SXR_plotmap_idxs={ECE_SXR_plotmap_idxs}")
    
    for jj, tstep in enumerate(NNjobs):
        i = ECE_SXR_plotmap_idxs[jj]        
        
        """ # fig A not needed anymore?
        # print("Saving plots: %g/%g" %(i+1,len(NNjobs)),end='\r')
        figA = Figure((8*img_size/6.,img_size))
        FigureCanvas(figA)
        # figA.set_size_inches(8,6)
        axA = figA.add_subplot(111)
        
        axA.get_yaxis().set_tick_params(direction='in')
        axA.get_xaxis().set_tick_params(direction='in')
        
        contoursA = axA.contourf(
            tTrace_arr[i], rrTrace_arr[i], TeTrace_arr[i], vmin=vminTe, vmax=vmaxTe, levels=contourNN_A, cmap="jet")
        axA.contour(
            tTrace_arr[i], rrTrace_arr[i], TeTrace_arr[i], vmin=vminTe, vmax=vmaxTe, levels=contourNN_A, cmap="binary")

        cbar = figA.colorbar(contoursA, ax=axA)
        cbar.ax.tick_params(labelsize=8, rotation=90) 
        cbar.ax.set_ylabel('Trad [eV]', rotation=90)
        axA.set_ylabel("R [m] (or rhop)")
        axA.set_xlabel("time [s]")
        axA.set_title("Shot: %g, t = %1.7f s" %(Shot,time_2Dprofiles[tstep]))
        
        filenameA = "TeTrace_A_%d_%04d.png"%(Shot,tstep)
        figA.savefig(path_tmp+filenameA,transparent=True, dpi=dpi)
        """
        # [osam]: for now don't save plots, that I don't use
        if (0 == 1):
            #########################################################
            ### Te2D
            #########################################################
            
            figTe = Figure((img_size,img_size))
            FigureCanvas(figTe)
            axTe = figTe.subplots(1, 1)
            
            normTe = Normalize(vmin=vminTe,vmax=vmaxTe)
            levelsTe = normTe.inverse(np.linspace(normTe(vminTe),normTe(vmaxTe),contourNN))
            prof_imgTe = axTe.contourf(R2D_arr[i], z2D_arr[i], Te2D_arr[i],norm=normTe,vmin=vminTe,vmax=vmaxTe, levels=levelsTe,cmap=my_cmap,extend=extend)
            CS_Te = axTe.contour(prof_imgTe, levels=prof_imgTe.levels,colors = 'k',linewidths=.2,  origin='lower')
            
            cTe = figTe.colorbar(prof_imgTe,extend=extend,ax=axTe)
            tick_locator = MaxNLocator(nbins=7)
            cTe.locator = tick_locator
            cTe.update_ticks()
            cTe.ax.set_position((0.76, 0.10, 0.03, 0.8))
            cTe.set_label('Trad [eV]', labelpad=4)
            
            axTe.set_xlabel('R [m]')
            axTe.set_ylabel('z [m]', labelpad=-5)
            axTe.set_aspect('equal', 'box')
            axTe.set_ylim([ymin, ymax])
            # axTe.set_xlim([xmin, xmax])
            axTe.set_title("Shot: %g, t = %1.7f s" %(Shot,time_2Dprofiles[i]), fontsize=11)

            filenameTe = "Te2D_%d_%04d.png"%(Shot,tstep)
            figTe.savefig(path_tmp+filenameTe,transparent=True, dpi=dpi,bbox_inches='tight')
            plt.close()
            
            #########################################################
            ### Te2Dfilt
            #########################################################
            figTeF = Figure((img_size,img_size))
            FigureCanvas(figTeF)
            axTeF = figTeF.subplots(1, 1)
            
            normTeF = Normalize(vmin=vminTe,vmax=vmaxTe)
            levelsTeF = normTeF.inverse(np.linspace(normTeF(vminTe),normTeF(vmaxTe),contourNN))
            prof_imgTeF = axTeF.contourf(r_SXR, z_SXR, Te2Dfilt_arr[i],norm=normTeF,vmin=vminTe,vmax=vmaxTe, levels=levelsTeF,cmap=my_cmap,extend=extend)
            CS_TeF = axTeF.contour(prof_imgTeF, levels=prof_imgTeF.levels,colors = 'k',linewidths=.2,  origin='lower')

            cTeF = figTeF.colorbar(prof_imgTeF,extend=extend,ax=axTeF)
            tick_locator = MaxNLocator(nbins=7)
            cTeF.locator = tick_locator
            cTeF.update_ticks()
            cTeF.ax.set_position((0.76, 0.10, 0.03, 0.8))
            cTeF.set_label('Trad [eV]', labelpad=4)
            
            axTeF.set_xlabel('R [m]')
            axTeF.set_ylabel('z [m]', labelpad=-5)
            axTeF.set_aspect('equal', 'box')
            axTeF.set_ylim([ymin, ymax])
            # axTeF.set_xlim([xmin, xmax])
            axTeF.set_title("Shot: %g, t = %1.7f s, sigmaGauss = %g" %(Shot,time_2Dprofiles[i], sigmaGauss), fontsize=11)

            filenameTeF = "Te2Dfilt_%d_%04d.png"%(Shot,tstep)
            figTeF.savefig(path_tmp+filenameTeF,transparent=True, dpi=dpi,bbox_inches='tight')
            plt.close()
            
        #########################################################
        ### emiss_SXR
        #########################################################

        # SXR plot
        if (SXRnormperturb_check == False):
            
            figSXR = Figure((img_size,img_size))
            FigureCanvas(figSXR)
            axSXR = figSXR.subplots(1, 1)
            
            normSXR = Normalize(vmin=vminSXR,vmax=vmaxSXR)
            levelsSXR = normSXR.inverse(np.linspace(normSXR(vminSXR),normSXR(vmaxSXR),contourNN))
            prof_imgSXR = axSXR.contourf(r_SXR,z_SXR,emiss_SXR[:,:,jj]*1e-3,norm=normSXR,vmin=vminSXR,vmax=vmaxSXR, levels=levelsSXR,cmap=my_cmap,extend=extend)
            CS_SXR = axSXR.contour(prof_imgSXR, levels=prof_imgSXR.levels,colors = 'k',linewidths=.2,  origin='lower')
            
            cSXR = figSXR.colorbar(prof_imgSXR,extend=extend,ax=axSXR)
            tick_locator = MaxNLocator(nbins=7)
            cSXR.locator = tick_locator
            cSXR.update_ticks()
            cSXR.ax.set_position((0.76, 0.10, 0.03, 0.8))
            cSXR.set_label('Emissivity SXR[kW$\cdot$m$^{-3}$]', labelpad=4)
            
            axSXR.set_xlabel('R [m]')
            axSXR.set_ylabel('z [m]', labelpad=-5)
            axSXR.set_aspect('equal', 'box')
            axSXR.set_ylim([ymin, ymax])
            # axSXR.set_xlim([xmin, xmax])
            axSXR.set_title("Shot: %g, t = %1.7f s" %(Shot,time_SXR[jj]), fontsize=11)

            filenameSXR = "emissSXR_%d_%04d.png"%(Shot,tstep)
            figSXR.savefig(path_tmp+filenameSXR,transparent=True, dpi=dpi,bbox_inches='tight')
            plt.close()
            
        if (SXRnormperturb_check == True):
            # figC = Figure((img_size,img_size))
            # FigureCanvas(figC)
            # axC = figC.subplots(1, 1)
            
            
            # contoursC = axC.contourf(r_SXR, z_SXR, perturb_emiss_norm[:,:,jj]*1e-3, vmin=vminSXR_prtb, vmax=vmaxSXR_prtb, levels=contourNN_C, cmap = "jet")
            
            # axC.contour(r_SXR, z_SXR, perturb_emiss_norm[:,:,jj]*1e-3, vmin=vminSXR_prtb, vmax=vmaxSXR_prtb, levels=contourNN_C, cmap = "binary")
            
            # cbarC = figC.colorbar(contoursC, ax=axC)
            # cbarC.ax.tick_params(labelsize=8, rotation=90) 
            # cbarC.ax.set_ylabel('Emiss - <Emiss> [kW/m^3]', rotation=90)
            # axC.set_ylabel("z [m]")
            # axC.set_xlabel("R [m]")
            # axC.set_title("Shot: %g, t = %1.7f s" %(Shot,time_SXR[jj]))
            
            # axC.set_xlim([xmin, xmax])
            # axC.set_ylim([ymin, ymax])

            # filenameC = "emissSXR_C_%d_%04d.png"%(Shot,tstep)
            # figC.savefig(path_tmp+filenameC,transparent=True, dpi=dpi)
            pass
     
        #########################################################
        ### emissBremss
        #########################################################
        figBr = Figure((img_size,img_size))
        FigureCanvas(figBr)
        axBr = figBr.subplots(1, 1)
        
        normBr = Normalize(vmin=vminBr,vmax=vmaxBr)
        levelsBr = normBr.inverse(np.linspace(normBr(vminBr),normBr(vmaxBr),contourNN))
        prof_imgBr = axBr.contourf(r_SXR,z_SXR,emBr2D_arr[i]*1e-3,norm=normBr,vmin=vminBr,vmax=vmaxBr, levels=levelsBr,cmap=my_cmap,extend=extend)
        CS_Br = axBr.contour(prof_imgBr, levels=prof_imgBr.levels,colors = 'k',linewidths=.2,  origin='lower')
        
        cBr = figBr.colorbar(prof_imgBr,extend=extend,ax=axBr)
        tick_locator = MaxNLocator(nbins=7)
        cBr.locator = tick_locator
        cBr.update_ticks()
        cBr.ax.set_position((0.76, 0.10, 0.03, 0.8))
        cBr.set_label('Emissivity Bremss [kW$\cdot$m$^{-3}$]', labelpad=4)
        
        axBr.set_xlabel('R [m]')
        axBr.set_ylabel('z [m]', labelpad=-5)
        axBr.set_aspect('equal', 'box')
        axBr.set_ylim([ymin, ymax])
        # axBr.set_xlim([xmin, xmax])
        axBr.set_title("Shot: %g, t = %1.7f s, sigmaGauss = %g" %(Shot,time_2Dprofiles[i], sigmaGauss), fontsize=11)

        filenameBr = "emissBr_%d_%04d.png"%(Shot,tstep)
        figBr.savefig(path_tmp+filenameBr,transparent=True, dpi=dpi,bbox_inches='tight')
        plt.close()
        
        #########################################################
        ### emissSXR - emissBremss 
        #########################################################
        
        figW = Figure((img_size,img_size))
        FigureCanvas(figW)
        axW = figW.subplots(1, 1)
        
        normW = Normalize(vmin=vminW,vmax=vmaxW)
        levelsW = normW.inverse(np.linspace(normW(vminW),normW(vmaxW),contourNN))
        prof_imgW = axW.contourf(r_SXR,z_SXR,(emiss_SXR[:,:,jj]-emBr2D_arr[i])*1e-3,norm=normW,vmin=vminW,vmax=vmaxW, levels=levelsW,cmap=my_cmap,extend=extend)
        CS_W = axW.contour(prof_imgW, levels=prof_imgW.levels,colors = 'k',linewidths=.2,  origin='lower')
        
        cW = figW.colorbar(prof_imgW,extend=extend,ax=axW)
        tick_locator = MaxNLocator(nbins=7)
        cW.locator = tick_locator
        cW.update_ticks()
        cW.ax.set_position((0.76, 0.10, 0.03, 0.8))
        cW.set_label('emSXR - emBremss [kW$\cdot$m$^{-3}$]', labelpad=4)
        
        axW.set_xlabel('R [m]')
        axW.set_ylabel('z [m]', labelpad=-5)
        axW.set_aspect('equal', 'box')
        axW.set_ylim([ymin, ymax])
        # axW.set_xlim([xmin, xmax])
        axW.set_title("Shot: %g, t = %1.7f s" %(Shot,time_SXR[jj]), fontsize=11)

        filenameW = "emissW_%d_%04d.png"%(Shot,tstep)
        figW.savefig(path_tmp+filenameW,transparent=True, dpi=dpi,bbox_inches='tight')
        plt.close()

    try:
        print("plot %g/%g(%2.1f%%)" %(NNjobs[jj], NN_2Dprofiles, (100*NNjobs[jj]/NN_2Dprofiles)), end='\r')
    except:
        pass









