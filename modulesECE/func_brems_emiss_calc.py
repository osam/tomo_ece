import numpy as np
from scipy.integrate import trapz
from scipy import interpolate
from pathlib import Path

"""
usage:
from func_brems_emiss_calc import calc_brems
brems_emissivity = calc_brems(Te,Zeff,ne) # [W/m3]
necessary files: "f_E_Be75.npz" (absorbtion probability of sxr diodes),
"u_gamma_gauntfactor.npz" (Sutherland's gaunt factor table data)
"""
global minTe
minTe = 100.0

def calc_brems(Te, Zeff, ne):
    """ Te in [eV], ne in [m-3] """


    path_current_file = str(Path(__file__).parent.absolute())
    
    hnu = np.arange(100.0, 5.0e4, 50)  # eV, photon energy spectrum

    data_loaded_absprob = np.load(path_current_file+"/f_E_Be75.npz")
    E_absprob, f_E_absprob = data_loaded_absprob["E"], data_loaded_absprob["f_E"]

    data_loaded_gaunt = np.load(path_current_file+"/u_gamma_gauntfactor.npz")
    u_table, gamma_table, gaunt_factor_table = (
        data_loaded_gaunt["u"],
        data_loaded_gaunt["gamma"],
        data_loaded_gaunt["gaunt_factor"],
    )
    # sxr filter
    if (Te>=minTe):
        gamma_calc = (Zeff**2) * 13.6056919 / Te
        u_calc = hnu / Te

        f_gaunt = interpolate.interp2d(
            u_table, gamma_table, gaunt_factor_table, kind="linear"
        )
        gaunt_interp = f_gaunt(np.log10(u_calc), np.log10(gamma_calc))

        f_absprob = interpolate.interp1d(E_absprob, f_E_absprob)
        absprob_interp = f_absprob(hnu)
        absprob_interp = 1.0

        brems_spectr = (
            1.42e-27
            * (Zeff**2)
            * np.sqrt(Te * 11604.505)
            * 1.0e-7
            * gaunt_interp
            * np.exp(-u_calc)
            / Te
        )  # [W*cm3/eV]

        # from cm-3 to m3 #
        brems_spectr = brems_spectr * 1e-6  # [W*m3/eV]

        spectr_to_integr = absprob_interp * (ne**2) * brems_spectr  # [W/(eV*m3)]

        emissivity = trapz(spectr_to_integr, hnu) # [W/m3]
    else:
        emissivity = 0.0 # [W/m3]
    return emissivity
