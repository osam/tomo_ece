import numpy as np
import pickle
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvas as FigureCanvas
from scipy.interpolate import griddata # try interpolation
from scipy.ndimage import gaussian_filter # 2D fulter gaussian
path = "/afs/ipp-garching.mpg.de/home/o/osam/tomography/tmp/"
fileload = "TestOutputECE.pickle"

with open(path + fileload, 'rb') as handle:
    outputECE = pickle.load(handle)
    
(
    R2D_arr,
    z2D_arr,
    Te2D_arr,
    tTrace_arr,
    rrTrace_arr,
    TeTrace_arr,
    NN_2Dprofiles,
    time_2Dprofiles,
    ECE_SXR_plotmap_idxs,
    path_tmp,
    Shot,
    r_SXR,
    z_SXR,
    emiss_SXR,
    time_SXR
) = (
    outputECE["R2D_arr"],
    outputECE["z2D_arr"],
    outputECE["Te2D_arr"],
    outputECE["tTrace_arr"],
    outputECE["rrTrace_arr"],
    outputECE["TeTrace_arr"],
    outputECE["NN_2Dprofiles"],
    outputECE["time_2Dprofiles"],
    outputECE["ECE_SXR_plotmap_idxs"],
    outputECE["path_tmp"],
    outputECE["Shot"],
    outputECE["r_SXR"],
    outputECE["z_SXR"],
    outputECE["emiss_SXR"],
    outputECE["time_SXR"] 
)
    
# time index
i = int(0)

# Interpolate the data using griddata
r_int, z_int = np.meshgrid(r_SXR, z_SXR)
Te_int = griddata((R2D_arr[i].flatten(), z2D_arr[i].flatten()), Te2D_arr[i].flatten(), (r_int, z_int), method='cubic', fill_value = 0.0)

Te_int[Te_int < 0.0] = 0.0
sigma = 1.6
Te_filt = gaussian_filter(Te_int,sigma,mode="nearest")

img_size = 6
dpi = 96
contourNN_B = 40
contourNN_C = 20
contourNN_I = 40
xmin, xmax = 1.2, 2.05
ymin, ymax = -0.5, 0.6


# figB = Figure((img_size,img_size))
# FigureCanvas(figB)
# axB = figB.subplots(1, 1)
figB, axB = plt.subplots(figsize=(img_size,img_size))

contoursB = axB.contourf(R2D_arr[i], z2D_arr[i], Te2D_arr[i], contourNN_B, cmap = "jet")

axB.contour(R2D_arr[i], z2D_arr[i], Te2D_arr[i], contourNN_B, cmap = "binary")
cbarB = figB.colorbar(contoursB, ax=axB)
cbarB.ax.tick_params(labelsize=8, rotation=90) 
cbarB.ax.set_ylabel('Trad [eV]', rotation=90)
axB.set_ylabel("z [m]")
axB.set_xlabel("R [m]")
axB.set_title("Shot: %g, t = %0.6g s" %(Shot,time_2Dprofiles[i]))

axB.set_xlim([xmin, xmax])
axB.set_ylim([ymin, ymax])
# axB.axis('scaled')

# figC = Figure((img_size,img_size))
# FigureCanvas(figC)
# axC = figC.subplots(1, 1)
figC, axC = plt.subplots(figsize=(img_size,img_size))


contoursC = axC.contourf(r_SXR, z_SXR, emiss_SXR[:,:,i]*1e-3, contourNN_C, cmap = "jet")

axC.contour(r_SXR, z_SXR, emiss_SXR[:,:,i]*1e-3, contourNN_C, cmap = "binary")

cbarC = figC.colorbar(contoursC, ax=axC)
cbarC.ax.tick_params(labelsize=8, rotation=90) 
cbarC.ax.set_ylabel('Emissivity [kW/m^3]', rotation=90)
axC.set_ylabel("z [m]")
axC.set_xlabel("R [m]")
axC.set_title("Shot: %g, t = %0.6g s" %(Shot,time_SXR[i]))

axC.set_xlim([xmin, xmax])
axC.set_ylim([ymin, ymax])



figI, axI = plt.subplots(figsize=(img_size,img_size))


# contoursI = axI.contourf(r_int, z_int, Te_int, contourNN_I, cmap = "jet")
contoursI = axI.contourf(r_int, z_int, Te_filt, contourNN_I, cmap = "jet")

# axI.contour(r_int, z_int, Te_int, contourNN_I, cmap = "binary")
axI.contour(r_int, z_int, Te_filt, contourNN_I, cmap = "binary")

cbarI = figI.colorbar(contoursI, ax=axI)
cbarI.ax.tick_params(labelsize=8, rotation=90) 
cbarI.ax.set_ylabel('Trad [eV]', rotation=90)
axI.set_ylabel("z [m]")
axI.set_xlabel("R [m]")
axI.set_title("Shot: %g, t = %0.6g s" %(Shot,time_SXR[i]))

axI.set_xlim([xmin, xmax])
axI.set_ylim([ymin, ymax])



plt.show()

